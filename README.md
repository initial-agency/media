# Media

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)

## Install

Via Composer

``` bash
$ composer require initial/media
```

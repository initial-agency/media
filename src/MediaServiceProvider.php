<?php
namespace Initial\Media;

use \Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = false;

    public function boot()
    {
        //
    }
}
